package Lab3;

import java.io.*;
import java.net.*;
import java.util.Scanner;

public class ClientContext implements StateMachine {
	public int SOCKET_TIMEOUT_MS = 1000;
	public int TIMEOUT_MAX = 10;
	public final int PACKET_SIZE = 512;
	public int MAX_COMMAND_SIZE = 100;
	public int TEST = 1;
	private State currentState;
	private DatagramSocket clientSocket;
	private static boolean fileDownloadInProgress;
	private static DatagramPacket packet, lastPacket;
	private static InetAddress address;
	private static int seqNum = 0;
	private static String fileName = null;
	private static boolean fileSendInProgress;
	private int port = 4445;
	boolean quit = false;

	public void run() throws Exception {
		System.out.println("New Client Context...");
		currentState = new IdleState(this);
		address = InetAddress.getByName("localhost");

		BufferedReader stdin;
		clientSocket = new DatagramSocket(port);

		while (!quit) {
			System.out.print("Client >>> ");
			stdin = new BufferedReader(new InputStreamReader(System.in));
			handleUserInput(stdin.readLine());
		}

	}

	private void handleUserInput(String userInput) {
		String[] command = new String[MAX_COMMAND_SIZE];
		command = userInput.split(" ");
		try {
			switch (command[0]) {
			case "get,":
				if (TEST == 1) {
					System.out.println("getting...");
					System.out.println("Filename: " + command[1]);
				}
				ClientContext.fileName = command[1];
				this.setState(new SendState(this));
				currentState.readRequest(command[1]); // send read request
				ClientContext.fileDownloadInProgress = true;
				this.setState(new ReceiveState(this));
				currentState.waitResponse(); // this will go into a loop,
												// receiving data and sending
												// acks until file downloaded
				this.setState(new IdleState(this));

				return;
			case "put,":
				if (TEST == 1) {
					System.out.println("putting...");
					System.out.println("Filename: " + command[1]);
				}
				ClientContext.fileName = command[1];
				this.setState(new SendState(this));
				currentState.writeRequest(command[1]); // send write request
				this.setState(new ReceiveState(this));
				currentState.waitResponse();
				this.setState(new IdleState(this));
				break;
			case "help":
				printHelp();
				// continue;
			case "settings":
				changeSettings();
				// continue;
			case "quit":
				if (TEST == 1)
					System.out.println("quitting");
				clientSocket.close();
				quit = true;
				return;
			default:
				System.err.println("Invalid input");
				// continue;
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	@SuppressWarnings("unused")
	private void changeSettings() {
		Scanner stdin = new Scanner(System.in);
		int selection;
		System.out.println("Change SETTINGS");
		System.out.println("1. TEST = " + TEST);
		System.out.println("2. Socket timeout = " + SOCKET_TIMEOUT_MS + "ms");
		System.out.println("3. Max packets wait = " + TIMEOUT_MAX);
		System.out.println("4. Max command parameters = " + MAX_COMMAND_SIZE);
		System.out.println("5. Packet size = " + PACKET_SIZE);
		System.out.println("6. EXIT SETTINGS");
		System.out.print("Setting to change >> ");
		selection = stdin.nextInt();
		switch (selection) {
		case 1:
			System.out.println("Changing TEST...");
			break;
		case 2:
			System.out.println("Changing SOCKET_TIMEOUT_MS");
			break;
		case 3:
			System.out.println("Changing TIMEOUT_MAX");
			break;
		case 4:
			System.out.println("Changing MAX_COMMAND_SIZE");
			break;
		case 5:
			System.out.println("Cannot change PACKET_SIZE");
			break;
		case 6:
			break;
		}
		stdin.close();
	}

	public void timeout() {
		try {
			System.out.println("Timeout: Resending last packet");
			this.setState(new IdleState(this));
			this.setState(new SendState(this));
			currentState.resendLastPacket();
			this.setState(new IdleState(this));
		} catch (Exception e) {
		}
	}

	private void printHelp() {
		BufferedReader inFromUser = new BufferedReader(new InputStreamReader(
				System.in));
		int option;
		System.err
				.print("Proper syntax is: [command], [filename]  \n1. Show commands \n2. Exit help \n>>");
		try {
			option = inFromUser.read();
			switch (option) {
			case '1':
				System.err.println("Commands: get, put,");
			case '2':
				return;
			default:
				return;
			}
		} catch (IOException e) {

		}
	}

	@Override
	public void setState(State s) {
		currentState = s;
	}

	public State getState() {
		return currentState;
	}

	@Override
	public DatagramSocket getSocket() {
		return clientSocket;
	}

	@Override
	public InetAddress getAddress() {
		return address;
	}

	@Override
	public int getPort() {
		return port;
	}

	public DatagramPacket getPacket() {
		return packet;
	}

	public void setPacket(DatagramPacket packet) {
		ClientContext.packet = packet;
	}

	public DatagramPacket getLastPacket() {
		return lastPacket;
	}

	public void setLastPacket(DatagramPacket lastPacket) {
		ClientContext.lastPacket = lastPacket;
	}

	@Override
	public void handleAck(int seqNum) {
		if (!ClientContext.fileSendInProgress) { // if not already sending a
													// file
			ClientContext.fileSendInProgress = true;
			ClientContext.seqNum = 0;
			this.setState(new SendState(this));
			this.sendFile("clientFiles\\output\\" + fileName);
			this.setState(new IdleState(this));
			ClientContext.fileSendInProgress = false;
		} else if (ClientContext.fileSendInProgress) {
			ClientContext.seqNum = seqNum;
		}

	}

	public void handleData(int seqNum, byte[] data) {
		try {

			boolean append = false;
			if (seqNum == 1) {
				append = false;
			} else if (seqNum > 1) {
				append = true;
			}

			if (!ClientContext.fileDownloadInProgress) { // file sending not
															// already in
				// progress
				this.setState(new SendState(this));
				currentState.sendError(0);
				this.setState(new IdleState(this));
				return;
			} else if (seqNum != ClientContext.seqNum + 1) { // not subsequent
																// packet
				this.setState(new SendState(this));
				currentState.sendAck(ClientContext.seqNum);
				this.setState(new ReceiveState(this));
				currentState.waitResponse();
				this.setState(new IdleState(this));
				return;
			} else { // as long as no errors, continue
				ClientContext.seqNum = seqNum; // update sequence number
				this.setState(new ReceiveState(this));
				currentState.transfer("clientFiles//input//"
						+ ClientContext.fileName, data, append);
				this.setState(new SendState(this));
				try {
					currentState.sendAck(seqNum);
					if (!isLastPacketOfFile(data)) {
						this.setState(new ReceiveState(this));
						currentState.waitResponse();
						this.setState(new IdleState(this));
						return;
					} else {
						ClientContext.seqNum = 0;
						this.setState(new IdleState(this));
						return;
					}
				} catch (IOException e) {
					e.printStackTrace();
				}

			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	private boolean isLastPacketOfFile(byte[] data) {
		if (data.length < 506)
			return true;
		else
			return false;
	}

	@Override
	public void handleError(int seqNum) {
		System.err.println("Received error from server: " + seqNum);
		if (seqNum == -1)
			System.err.println("Server does not have file requested");
		else if (seqNum == 0)
			ClientContext.seqNum = seqNum;
		else
			try {
				this.setState(new SendState(this));
				currentState.resendLastPacket();
				this.setState(new ReceiveState(this));
			} catch (IOException e) {
				e.printStackTrace();
			}

	}

	@Override
	public void handleWriteRequest(String fileName) {
		// System.err.println("Received write Request from server: " +
		// fileName);
		// this.setState(new SendState(this));
		// try {
		// currentState.sendError("Received write Request from server: "
		// + fileName);
		// } catch (IOException e) {
		// e.printStackTrace();
		// }
		// this.setState(new IdleState(this));
	}

	@Override
	public void handleReadRequest(String fileName) {
		// System.err.println("Received read Request from server: " + fileName);
		// this.setState(new SendState(this));
		// try {
		// currentState.sendError("Received read Request from server: "
		// + fileName);
		// } catch (IOException e) {
		// e.printStackTrace();
		// }
		// this.setState(new IdleState(this));
	}

	public void sendFile(String filePath) {
		try {

			File file = new File(filePath);
			FileInputStream fileInputStream = new FileInputStream(file);
			String filename = filePath.substring(filePath.indexOf("put\\") + 4)
					+ " ";
			int fileSize = (int) file.length();
			int iterations = (fileSize / 506) + 1;
			byte[] buffer;
			System.out.println("Sending " + filename + "over " + iterations
					+ " packet(s)...");
			for (int i = 1; i <= iterations; i++) {
				buffer = new byte[506];
				int bytesRead = fileInputStream.read(buffer);
				if (bytesRead < 506 && bytesRead > -1) {
					byte[] newBuff = new byte[bytesRead];
					System.arraycopy(buffer, 0, newBuff, 0, bytesRead);
					buffer = newBuff;
				} else if ((bytesRead == 506) && (i == iterations)) {
					iterations = iterations + 1;
				} else if (bytesRead == -1) {
					buffer = " ".getBytes();
					iterations--;
				}
				currentState.sendData(buffer, i);
				this.setState(new ReceiveState(this));
				currentState.waitResponse();
				this.setState(new SendState(this));
				if (ClientContext.seqNum != i) {
					currentState.sendError(i);
					fileInputStream.close();
					return;
				}
			}
			System.out.println("File uploaded.");
			ClientContext.seqNum = 0;
			fileInputStream.close();
		} catch (FileNotFoundException e) {
			try {
				currentState.sendError(-1);
			} catch (IOException e1) {
				e1.printStackTrace();
			}
		} catch (IOException e) {
			System.err.println("Socket timeout on send...");
		}

	}
}
