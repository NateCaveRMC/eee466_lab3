package Lab3;

import java.io.*;
import java.nio.ByteBuffer;

public class IdleState implements State {
	private StateMachine context;

	IdleState(StateMachine ctx) {
		context = ctx;
		// context.setState(new IdleState(context));
	}

	@Override
	public void transfer(String filePath, byte[] data, boolean append) {
		// TODO Auto-generated method stub

	}

	@Override
	public void waitResponse() throws IOException {
		// TODO Auto-generated method stub

	}

	@Override
	public void readRequest(String fileName) throws IOException {
		// TODO Auto-generated method stub

	}

	@Override
	public void sendData(byte[] data, int seqNum) throws IOException {
		// TODO Auto-generated method stub

	}

	@Override
	public void sendAck(int seqNum) throws IOException {
		// TODO Auto-generated method stub

	}

	@Override
	public void writeRequest(String fileName) throws IOException {
		// TODO Auto-generated method stub

	}

	@Override
	public void writeToOutputStream(ByteBuffer bytes) throws IOException {
		// TODO Auto-generated method stub

	}

	@Override
	public void resendLastPacket() throws IOException {
		// TODO Auto-generated method stub

	}

	@Override
	public void sendError(int seqNum) throws IOException {
		// TODO Auto-generated method stub

	}

}
