package Lab3;

import java.io.*;
import java.net.DatagramPacket;
import java.net.SocketTimeoutException;
import java.nio.ByteBuffer;

public class ReceiveState implements State {
	private StateMachine context;

	private ByteBuffer input = ByteBuffer.allocate(512);
	private DatagramPacket packet = new DatagramPacket(input.array(),
			input.array().length);

	ReceiveState(StateMachine ctx) {
		context = ctx;
	}

	@Override
	public void sendError(int seqNum) {
		System.err.println("Called sendError() on ReceiveState");
	}

	public void transfer(String filePath, byte[] data, boolean append) {

		try {
			FileOutputStream fos = new FileOutputStream(filePath, append);
			fos.write(data);
			fos.close();
		} catch (IOException e) {
		}
	}

	public void waitResponse() throws IOException {

		int timeoutCount = 0;
		int SOCKET_TIMEOUT_MS = 1000;
		int TIMEOUT_MAX = 10;
		int TEST = 1;
		int seqNum;
		byte[] temp;
		String fileName;

		while (timeoutCount < TIMEOUT_MAX) {
			context.getSocket().setSoTimeout(SOCKET_TIMEOUT_MS);
			try {
				context.getSocket().receive(packet);
				// trim input
				int sizeOfPacket = packet.getLength();
				if (sizeOfPacket < 512) {
					System.out.println("Size of packet: " + sizeOfPacket);
					ByteBuffer newInput = ByteBuffer.allocate(sizeOfPacket);
					newInput.put(input.array(), 0, sizeOfPacket);
					input = ByteBuffer.allocate(sizeOfPacket);
					input.put(newInput.array(), 0, sizeOfPacket);
				}
				timeoutCount = TIMEOUT_MAX + 1;
			} catch (SocketTimeoutException e) {
				// resend last request on timeout
				context.timeout();
				timeoutCount++;
			}
		}
		if (timeoutCount > TIMEOUT_MAX) {
			// parse the reply
			input.position(0);
			char character = input.getChar();
			switch (character) {

			case 'D':
				seqNum = input.getInt();
				System.out.println("Received data packet " + seqNum + "!");
				temp = new byte[input.remaining()];
				input.get(temp, 0, temp.length);
				context.handleData(seqNum, temp);
				return;

			case 'A':
				seqNum = input.getInt();
				System.out.println("Received ack " + seqNum + "!");
				context.handleAck(seqNum);
				return;

			case 'E':
//				temp = new byte[input.remaining()];
//				input.get(temp, 0, input.remaining());
//				String errorMessage = new String(temp);
				seqNum = input.getInt();
				System.err.println("Received error: "+seqNum);
				context.handleError(seqNum);
				return;

			case 'W':
				System.out.println("Received write request!");
				temp = new byte[input.remaining()];
				input.get(temp, 0, input.remaining());
				fileName = new String(temp);
				context.handleWriteRequest(fileName);
				return;

			case 'R':
				System.out.println("Received write request!");
				temp = new byte[input.remaining()];
				input.get(temp, 0, input.remaining());
				fileName = new String(temp);
				context.handleReadRequest(fileName);
				return;
			default:
				System.err.println("Error, not a known message type");
				if (TEST == 1)
					System.out.println("Message: " + new String(input.array()));
				return;
			}

		} else
			System.out.println("Unable to establish a connection");
		return;

	}

	// private String[] seperateString(String str, String delim) {
	// String[] data = new String[4];
	// // packet number
	// data[0] = (String) str.substring(0, str.indexOf(" "));
	// str = str.substring(str.indexOf(" ") + 1);
	// // total packets being sent
	// data[1] = (String) str.substring(0, str.indexOf(" "));
	// str = str.substring(str.indexOf(" ") + 1);
	// // filename
	// data[2] = str.substring(0, str.indexOf(" "));
	// str = str.substring(str.indexOf(" ") + 1);
	// // the data
	// return data;
	// }

	@Override
	public void readRequest(String string) throws IOException {
		// TODO Auto-generated method stub

	}

	@Override
	public void writeRequest(String string) throws IOException {
		// TODO Auto-generated method stub

	}

	@Override
	public void resendLastPacket() throws IOException {
		// TODO Auto-generated method stub

	}

	@Override
	public void sendAck(int seqNum) throws IOException {
		// TODO Auto-generated method stub

	}

	@Override
	public void sendData(byte[] data, int seqNum) throws IOException {
		// TODO Auto-generated method stub

	}

	@Override
	public void writeToOutputStream(ByteBuffer bytes) throws IOException {
		// TODO Auto-generated method stub

	}

}
