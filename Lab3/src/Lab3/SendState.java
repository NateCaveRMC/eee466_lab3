package Lab3;

import java.io.*;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.nio.ByteBuffer;

public class SendState implements State {
	private StateMachine context;

	SendState(StateMachine ctx) {
		context = ctx;
	}

	public void readRequest(String fileName) throws IOException {
//		System.out.println("Entered readRequest() method...");
		ByteBuffer output = ByteBuffer.allocate(512);
		output.putChar('R');
		output.put(fileName.getBytes());
		writeToOutputStream(output);
		System.out.println("Sent read request!");

	}

	public void sendData(byte[] data, int seqNum) throws IOException {
//		System.out.println("Entered sendData() method...");
		ByteBuffer output = ByteBuffer.allocate(6+data.length);
//		System.out.println("data length: " + data.length);
		output.putChar('D'); // 2 bytes for a char
		output.putInt(seqNum); // 4 bytes for an int
		output.put(data); // incoming data is 506 bytes
		writeToOutputStream(output);
		System.out.println("Sent data packet " + seqNum);
	}

	public void sendAck(int seqNum) throws IOException {
//		System.out.println("Entered sendAck() method...");
		ByteBuffer output = ByteBuffer.allocate(512);
		output.putChar('A');
		output.putInt(seqNum);
		writeToOutputStream(output);
		System.out.println("Sent ack " + seqNum + "!");
	}

	public void sendError(int seqNum) throws IOException {
//		System.out.println("Entered sendError() method...");
		ByteBuffer output = ByteBuffer.allocate(512);
		output.putChar('E');
		output.putInt(seqNum);
//		output.put(details.getBytes());
		writeToOutputStream(output);
//		System.err.print("Sent error: "+ details);
	}

	public void writeRequest(String fileName) throws IOException {
//		System.out.println("Entered writeRequest() method...");
		ByteBuffer output = ByteBuffer.allocate(512);
		output.putChar('W');
		output.put(fileName.getBytes());
		writeToOutputStream(output);
		System.out.println("Sent write request!");
	}

	public void writeToOutputStream(ByteBuffer bytes) throws IOException {
//		 System.out.println("Entered writeToOutputStream() method...");
//		System.out.println("bytes length: "+bytes.array().length);
		DatagramPacket packet = new DatagramPacket(bytes.array(),
				bytes.array().length, context.getAddress(), 1234);
		context.setLastPacket(packet); // create history	
		context.getSocket().send(packet); // send packet

	}

	public void resendLastPacket() throws IOException {
//		System.out.println("Entered resendLastPacket() method...");
		context.getSocket().send(context.getLastPacket()); // send packet
		System.out.println("Resent last packet!");

	}

	@Override
	public void transfer(String filePath, byte[] data, boolean append) {
		System.err.println("Called transfer() in SendState");
		return;
	}

	@Override
	public void waitResponse() throws IOException {
		System.err.println("Called waitResponse() in SendState");
		return;
	}

}
