package Lab3;

import java.io.*;
import java.net.*;
import java.nio.*;

public class ServerContext implements StateMachine {
	public int TEST = 1;

	private State currentState;
	private static DatagramSocket socket = null;
	private static BufferedReader in = null;
	public InetAddress address;
	public int port = 7721;
	private ByteBuffer buff;
	private ByteBuffer output;
	private DatagramPacket inFromClient;
	private static boolean fileSendInProgress = false;
	private static boolean fileReceiveInProgress = false;
	private static int seqNum = 0;

	private static String fileName;

	private static DatagramPacket lastPacket;

	@Override
	public void run() throws Exception {
		System.out.println("New Server Context...");
		socket = new DatagramSocket(port);
		while (true) {
			currentState = new IdleState(this);
			checkClientInput(); // expects only a read or write
		}

	}

	private void checkClientInput() {
		try {
			init();
			char inputChar = buff.getChar();
			byte[] temp = new byte[buff.remaining()];
			buff.get(temp, 0, buff.remaining());
			String fileName = "";
			ServerContext.seqNum = 0;
			System.out.println("Packet contained: " + inputChar);

			switch (inputChar) {
			case 'R':
				fileName = new String(temp).trim();
				if (TEST == 1) {
					System.out.println("Got a read request");
					System.out.println("Filename: " + fileName);
				}
				this.handleReadRequest(fileName);
				break;
			case 'W':
				fileName = new String(temp).trim();
				if (TEST == 1) {
					System.out.println("Got a write request");
					System.out.println("Filename: " + fileName);
				}
				this.handleWriteRequest(fileName);
				break;
			/*
			 * case 'D': if (TEST == 1) { System.err.println("Got data");
			 * System.err.println("Data: " + new String(temp)); } break;
			 */
			/*
			 * case 'A': System.err.println("Got an acknowledge"); break;
			 */
			// case 'E':
			// int seqNum = input.getInt();
			// this.handleError(seqNum);
			// break;
			default:
				System.err.println("Error, not a known msg type");
				break;
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	private void init() throws IOException {
		buff = ByteBuffer.allocate(512);
		output = ByteBuffer.allocate(512);
		// receive request
		inFromClient = new DatagramPacket(buff.array(), buff.array().length);
		socket.setSoTimeout(0);
		socket.receive(inFromClient);
		// set address
		address = inFromClient.getAddress();
		// port = inFromClient.getPort();
	}

	@Override
	public void setState(State s) {
		currentState = s;
	}

	public State getState() {
		return currentState;
	}

	@Override
	public DatagramSocket getSocket() {
		return socket;
	}

	@Override
	public InetAddress getAddress() {
		return address;
	}

	public int getPort() {
		return port;
	}

	@Override
	public void setLastPacket(DatagramPacket packet) {
		ServerContext.lastPacket = packet;

	}

	@Override
	public DatagramPacket getLastPacket() {
		return ServerContext.lastPacket;
	}

	@Override
	public void timeout() {
		try {
			System.out.println("Timeout: Resending last packet");
			this.setState(new IdleState(this));
			this.setState(new SendState(this));
			currentState.resendLastPacket();
			this.setState(new IdleState(this));
		} catch (Exception e) {
		}
	}

	@Override
	public void handleAck(int seqNum) {
		ServerContext.seqNum = seqNum;
		return;

	}

	@Override
	public void handleData(int seqNum, byte[] data) {
		try {
			boolean append = false;
			if (seqNum == 1) {
				append = false;
			} else if (seqNum > 1) {
				append = true;
			}

			if (!ServerContext.fileReceiveInProgress) { // file sending not
														// already in progress
				this.setState(new SendState(this));
				currentState.sendError(0);
				this.setState(new IdleState(this));
				return;
			} else if (seqNum != ServerContext.seqNum + 1) { // not subsequent
																// packet
				this.setState(new SendState(this));
				// currentState.sendError(ServerContext.seqNum+1);
				currentState.sendAck(ServerContext.seqNum);
				this.setState(new ReceiveState(this));
				currentState.waitResponse();
				this.setState(new IdleState(this));
				return;
			} else { // as long as no errors, continue
				this.setState(new ReceiveState(this));
				currentState.transfer("serverFiles\\input\\" + fileName, data,
						append);
				this.setState(new SendState(this));

				currentState.sendAck(seqNum);
				ServerContext.seqNum = seqNum; // update sequence number
				if (!isLastPacketOfFile(data)) {
					this.setState(new ReceiveState(this));
					currentState.waitResponse();
					this.setState(new IdleState(this));
					return;
				} else {
					ServerContext.seqNum = 0;
					this.setState(new IdleState(this));
					return;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	private boolean isLastPacketOfFile(byte[] data) {
		if (data.length < 506)
			return true;
		else
			return false;
	}

	@Override
	public void handleError(int seqNum) {
		if (seqNum == -1)
			System.err.println("Client does not have file requested");
		else if (seqNum == 0)
			ServerContext.seqNum = seqNum;
		else
			try {
				this.setState(new SendState(this));
				currentState.resendLastPacket();
				this.setState(new ReceiveState(this));
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	}

	@Override
	public void handleWriteRequest(String fileName) {
		ServerContext.fileName = fileName;
		try {
			if (!ServerContext.fileReceiveInProgress) {
				ServerContext.fileReceiveInProgress = true;
				ServerContext.seqNum = 0;
				this.setState(new SendState(this));
				currentState.sendAck(0);
				this.setState(new ReceiveState(this));
				currentState.waitResponse();
				this.setState(new IdleState(this));
				ServerContext.fileReceiveInProgress = false;
			} else if (ServerContext.fileReceiveInProgress) {
				this.setState(new SendState(this));
				currentState.sendError(0);
				this.setState(new IdleState(this));
				return;
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void handleReadRequest(String fileName) {
		ServerContext.fileName = fileName;
		try {
			if (!ServerContext.fileSendInProgress) {
				ServerContext.fileSendInProgress = true;
				ServerContext.seqNum = 0;
				this.setState(new SendState(this));
				this.sendFile("serverFiles\\output\\" + fileName);
				this.setState(new IdleState(this));
				ServerContext.fileSendInProgress = false;
			} else if (ServerContext.fileSendInProgress) {
				this.setState(new SendState(this));
				currentState.sendError(0);
				this.setState(new IdleState(this));
				return;
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void sendFile(String filePath) {
		try {

			File file = new File(filePath);
			FileInputStream fileInputStream = new FileInputStream(file);
			String filename = filePath.substring(filePath.indexOf("put\\") + 4)
					+ " ";
			int fileSize = (int) file.length();
			int iterations = (fileSize / 506) + 1;
			byte[] buffer;
			System.out.println("Sending " + filename + "over " + iterations
					+ " packet(s)...");
			for (int i = 1; i <= iterations; i++) {
				buffer = new byte[506];
				int bytesRead = fileInputStream.read(buffer);
				if (bytesRead < 506 && bytesRead > -1) {
					byte[] newBuff = new byte[bytesRead];
					System.arraycopy(buffer, 0, newBuff, 0, bytesRead);
					buffer = newBuff;
				} else if ((bytesRead == 506) && (i == iterations)) {
					iterations = iterations + 1;
				} else if (bytesRead == -1) {
					buffer = " ".getBytes();
					iterations--;
				}
				currentState.sendData(buffer, i);
				this.setState(new ReceiveState(this));
				currentState.waitResponse();
				this.setState(new SendState(this));
				if (ServerContext.seqNum != i) {
					currentState.sendError(i);
					fileInputStream.close();
					return;
				}
			}
			System.out.println("File uploaded.");
			ServerContext.seqNum = 0;
			fileInputStream.close();
		} catch (FileNotFoundException e) {
			try {
				currentState.sendError(-1);
			} catch (IOException e1) {
				e1.printStackTrace();
			}
		} catch (IOException e) {
			System.err.println("Socket timeout on send...");
		}

	}

}
