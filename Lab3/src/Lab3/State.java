package Lab3;

import java.io.*;
import java.nio.ByteBuffer;

public interface State {
	static int BUFFER_SIZE = 512;
	public void transfer(String filePath, byte[] data, boolean append);
	public void waitResponse() throws IOException;
	public void readRequest(String fileName) throws IOException;
	public void sendData(byte[] data, int seqNum) throws IOException;
	public void sendAck(int seqNum) throws IOException;
	public void sendError(int seqNum) throws IOException;
	public void writeRequest(String fileName) throws IOException;
	public void writeToOutputStream(ByteBuffer bytes) throws IOException;
	public void resendLastPacket() throws IOException;

	
}
