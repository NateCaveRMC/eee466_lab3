package Lab3;

import java.net.*;
import java.io.*;

public interface StateMachine {
	public static int PORT = 4445;

	public void setState(State s);

	// Other useful interfaces to manage communication streams
	// public Socket getSocket();
	//
	// public InputStream getInputStream() throws IOException;
	//
	// public OutputStream getOutputStream() throws IOException;

	public DatagramSocket getSocket();

	public InetAddress getAddress();

	public int getPort();

	public void run() throws Exception;

	public void setLastPacket(DatagramPacket packet);

	public DatagramPacket getLastPacket();

	public void timeout();

	public void handleAck(int seqNum);

	public void handleData(int seqNum, byte[] data);

	public void handleError(int seqNum);

	public void handleWriteRequest(String fileName);

	public void handleReadRequest(String fileName);

	public void sendFile(String filePath);

}
